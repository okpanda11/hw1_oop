package address;

public class Address {
    public static void main(String[] args) {
        Address MyAddress = new Address(100500, "Ukraine", "Kiyv", "Schevchenka", 11, 55);
        System.out.println("My index: " + MyAddress.index);
        System.out.println("My country: " + MyAddress.country);
        System.out.println("My city: " + MyAddress.city);
        System.out.println("My street: " + MyAddress.street);
        System.out.println("My house: " + MyAddress.house);
        System.out.println("My apartment: " + MyAddress.apartment);
    }

    private int index;
    private String country;
    private String city;
    private String street;
    private int house;
    private int apartment;

    //constructor:
    public Address(int index, String country, String city, String street, int house, int apartment) {
        this.index = index;
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
    }

    // getters & setters:
    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    public int getHouse() {
        return house;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

    public int getApartment() {
        return apartment;
    }
}
