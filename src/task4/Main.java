package task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Computer[] computers = new Computer[5];

        System.out.println("input models: ");

        for (int i = 0; i < 5; i++) {
            String model = sc.nextLine();
            computers[i] = new Computer(model);
        }

        System.out.println("Computer models: ");
        for (Computer a : computers) {
            System.out.print(a.getModel() + " ");
        }
        sc.close();
    }

}
